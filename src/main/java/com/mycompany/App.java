package com.mycompany;


import com.mycompany.demo.Card;
import com.mycompany.demo.Player;
import com.mycompany.demo.Rank;

import java.util.UUID;

public class App {
    public static void main(String[] args) {

        Player player = Player.builder().build();
        player.setPlayerId(UUID.randomUUID().toString());
        player.setPlayerName("John");
        player.setPlayerRank(Rank.LOWER);
        player.setCard(Card.HEARTS);
        System.out.println(player.getDetails());

    }
}


